# Rancher setup role 

This is a simple rancher installer, using ansible


## How to Run : 

1. Modify `ansible/inventory` by adding the VM ip
2. Modify the `rancher_url` inside `ansible/group_vars/all/vars`
3. Add the certificates inside the folder `ansible/rancher-role/files`
4. Run the following commands : 

```bash
$ ansible-galaxy install -r ansible/ansible_roles.yml
$ ansible-playbook -i ansible/inventory ansible/main.yml
```